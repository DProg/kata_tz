package main

import (
	"fmt"
	"strings"
)
func main() {

	var numbers map[string]int = map[string]int {

		"1":	1,
		"2":	2,
		"3":	3,
		"4":	4,
		"5":	5,
		"6":	6,
		"7":	7,
		"8":	8,
		"9":	9,
		"10":	10,

		"I":	1,
		"II":	2,
		"III":	3,
		"IV":	4,
		"V":	5,
		"VI":	6,
		"VII":	7,
		"VIII":	8,
		"IX":	9,
		"X":	10,
	}

	var a string;
	var b string;
	var do string;

	fmt.Scan(&a);
	fmt.Scan(&do);
	fmt.Scan(&b);

	var first bool = strings.Contains(a, "I") || strings.Contains(a, "V") || strings.Contains(a, "X");
	var second bool = strings.Contains(b, "I") || strings.Contains(b, "V") || strings.Contains(b, "X"); 


// Проверка условия

	// Если оба числа римские
	if first && second { 
	
		if do == "+" {
			fmt.Println(numbers[a] + numbers[b]);
		}

		if do == "-" {
			fmt.Println("Невозможно вычитание римских цифр");
		}

		if do == "*" {
			fmt.Println(numbers[a] * numbers[b]);
		}

		if do == "/" {
			fmt.Println(numbers[a] / numbers[b]);
		}
	
	// Eсли оба числа арабсике
	} else if !first && !second { 
	
		if do == "-" {
			fmt.Println(numbers[a] - numbers[b]);
		}
		
		if do == "+" {
			fmt.Println(numbers[a] + numbers[b]);
		}

		if do == "*" {
			fmt.Println(numbers[a] * numbers[b]);
		}

		if do == "/" {
			fmt.Println(numbers[a] / numbers[b]);
		}
	
	// Иначе
	} else {

		fmt.Println("Нельзя производить операции с разным типом цифр");
	
	}

}
